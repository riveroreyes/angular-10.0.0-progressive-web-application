export class TrackHttpError {
  constructor(
    public errNumber?: number,
    public message?: string,
    public friendlyMessage?: string) { }
}
