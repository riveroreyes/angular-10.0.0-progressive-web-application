import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { Character } from '@shared/interfaces/character.interface';
import { TrackHttpError } from '@shared/models/track-http-error';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(private http: HttpClient) { }

  searchCharacters(name = '', page = 1): Observable<Character[] | TrackHttpError> {
    const filter = '?page=' + page + '&name=' + name;
    return this.http.get<Character[]>(environment.baseUrlApi + filter)
      .pipe(catchError( err => this.handleHttpError(err)));
  }

  getDetails(id: number): Observable<Character | TrackHttpError> {
    return this.http.get<Character>(environment.baseUrlApi + id)
      .pipe(catchError(err => this.handleHttpError(err)));
  }

  private handleHttpError(error: HttpErrorResponse): Observable<TrackHttpError> {
    const dataError = new TrackHttpError();
    dataError.errNumber = error.status;
    dataError.message = error.statusText;
    dataError.friendlyMessage = 'An error ocurred retrienving data.';
    return throwError(dataError);

  }

}
