import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-search',
  template: `
    <input
      class="form-control mr-sm-2"
      type="text"
      placeholder="Busca a partir de 3 caracteres"
      aria-label="Search"
      #inputSearch
      autofocus
      (keyup)="OnSearch(inputSearch.value)"
      />
  `,
  styles: [`
    input {
      width: 100%;
    }
  `]
})
export class FormSearchComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  OnSearch(value: string) {
    console.log(value);

    if (value && value.length > 3) {
      this.router.navigate(['character-list'], {
        queryParams: { name: value }
      });
    }
  }

}
