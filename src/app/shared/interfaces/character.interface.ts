export interface Character {
  id: number;
  name: string;
  image: string;
  species: string;
  gender: string;
  created: string;
  status: 'vivo' | 'muerto' | 'no se conoce';
  location: any;
  episode: any;
  origin: any;
}
