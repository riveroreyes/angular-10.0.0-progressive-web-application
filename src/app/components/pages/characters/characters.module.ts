import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { CharacterDetailsComponent } from '@characters/character-details/character-details.component';
import { CharacterListComponent } from '@characters/character-list/character-list.component';
import { CharacterComponent } from '@characters/character.component';

const customsComponents = [
  CharacterDetailsComponent,
  CharacterListComponent,
  CharacterComponent,
];

@NgModule({
  declarations: [...customsComponents],
  imports: [
    CommonModule,
    RouterModule,
    InfiniteScrollModule,
  ],
  exports: [...customsComponents]
})
export class CharactersModule { }
