import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Character } from '@app/shared/interfaces/character.interface';

@Component({
  selector: 'app-character',
  template: `
      <div class="card">
        <div class="image">
            <img
              [src]="character.image"
              [alt]="character.name"
              class="card-img-top"
            />
        </div>
        <div class="card-inner">
          <div class="header">
            <a [routerLink]="[ '/character-details', character.id ]" *ngIf="!details; else sinDetails">
              <h2>{{character.name | slice: 0:15}}</h2>
            </a>
            <ng-template #sinDetails>
              <h2>{{character.name | slice: 0:15}}</h2>
            </ng-template>
            <h4 class="text-muted">
              {{ character.gender }}
            </h4>
            <h6 *ngIf="details"><small>Estado: </small>{{character.status}}</h6>
            <h6 *ngIf="details"><small>Origen: </small>{{character.origin.name}}</h6>
            <h6 *ngIf="details"><small>Ubicación: </small>{{character.location.name}}</h6>
            <small class="text-muted">{{ character.created | date }}</small>
          </div>
        </div>
      </div>
  `,
  styles: [`
  `],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CharacterComponent implements OnInit {
  @Input() character: Character;
  @Input() details = false;

  constructor() { }

  ngOnInit(): void { }
}
