import { Component, OnInit, OnDestroy } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';

import { Subscription, Observable } from 'rxjs';

import { Character } from '@shared/interfaces/character.interface';
import { CharacterService } from '@shared/services/character.service';
import { TrackHttpError } from '@shared/models/track-http-error';

@Component({
  selector: 'app-character-details',
  templateUrl: './character-details.component.html',
  styleUrls: ['./character-details.component.scss']
})
export class CharacterDetailsComponent implements OnInit, OnDestroy {

  private subs1: Subscription;
  private subs2: Subscription;
  character$: Observable<Character | TrackHttpError >;
  id: number;

  constructor(
    private characterService: CharacterService,
    private route: ActivatedRoute
  ) {
  }


  ngOnInit(): void {
    this.getCharactersByQuery();
  }

  getCharactersByQuery(): void {
    this.subs1 = this.route.params.subscribe(
      (params: Params) => {
        // tslint:disable-next-line: no-string-literal
        this.id = params['id'];
        this.character$ = this.characterService.getDetails(this.id);
      }
    );
  }

  OnBack(){
    window.history.back();
  }

  ngOnDestroy() {
    if (this.subs1) {
      this.subs1.unsubscribe();
    }
  }

}
