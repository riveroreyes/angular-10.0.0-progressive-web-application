import { Component, OnInit, OnDestroy, Inject, HostListener } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, NavigationEnd } from '@angular/router';
import { DOCUMENT } from '@angular/common';

import { Subscription } from 'rxjs';
import { take, filter } from 'rxjs/operators';

import { CharacterService } from '@shared/services/character.service';
import { Character } from '@shared/interfaces/character.interface';
import { TrackHttpError } from '@shared/models/track-http-error';

type RequestInfo = {
  next: string;
};

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.scss']
})
export class CharacterListComponent implements OnInit, OnDestroy {

  private subs1: Subscription;
  private subs2: Subscription;
  private subs3: Subscription;
  characters: Character[] = [];
  private pageNum = 1;
  private name: string;
  private hideScrollHeight = 200;
  private showScrollHeight = 500;
  public showGoUpButton = false;
  private info: RequestInfo = {
    next: null,
  };
  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;

  constructor(
    private characterService: CharacterService,
    private router: Router,
    private route: ActivatedRoute,
    @Inject(DOCUMENT) private document: Document,
  ) {
    this.onUrlChanged();
  }


  ngOnInit(): void {
    this.getCharactersByQuery();
  }

  @HostListener('window:scroll', [])
  onWindowScroll(): void {
    const yOffSet = window.pageYOffset;
    const sizes = (yOffSet || this.document.documentElement.scrollTop || this.document.body.scrollTop);
    if (sizes > this.showScrollHeight) {
      this.showGoUpButton = true;
    } else if (this.showGoUpButton && sizes < this.hideScrollHeight) {
      this.showGoUpButton = false;
    }
  }

  onScrollDown(): void {
    console.log('INFO', this.info);

    if (this.info.next) {
      this.pageNum++;
      this.getDataFromservice();
    }
  }



  OnScrollTop(): void {
    this.document.body.scrollTop = 0; // safari
    this.document.documentElement.scrollTop = 0;
  }

  private onUrlChanged(): void {
    this.subs3 = this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe(
        () => {
          this.characters = [];
          this.pageNum = 1;
          this.getCharactersByQuery();
        }
      );
  }

  getCharactersByQuery(): void {
    this.subs1 = this.route.queryParams.pipe(take(1)).subscribe(
      (params: ParamMap) => {
        // tslint:disable-next-line: no-string-literal
        this.name = params['name'];
        this.getDataFromservice();
      }
    );
  }

  getDataFromservice(): void {
    this.subs2 = this.characterService.searchCharacters(this.name, this.pageNum)
      .pipe(take(1))
      .subscribe((res: any) => {
        if (res?.results?.length) {
          const { info, results } = res;
          this.characters = [...results];
          this.info = info;
        } else {
          this.characters = [];
        }
      }, (err: TrackHttpError) => console.log(err.friendlyMessage)
      );
  }

  ngOnDestroy() {
    if (this.subs1) {
      this.subs1.unsubscribe();
    }
    if (this.subs2) {
      this.subs2.unsubscribe();
    }
    if (this.subs3) {
      this.subs3.unsubscribe();
    }
  }

}
