# Angular-10.0.0-progressive-web-application

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.how 

Show how build a simple PWA but very instructive application. 

## Thanks and credits
* [Dominicode](https://www.youtube.com/channel/UC3QuZuJr2_EOUak8bWUd74A)
* [Tutorial](https://www.youtube.com/watch?v=hGU2ceM8aVY&list=PL_9MDdjVuFjEKyoP8HMSld5PBcZYMwtdO) 


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## ScreenShots

<img src="./repo/figure1.jpeg">
